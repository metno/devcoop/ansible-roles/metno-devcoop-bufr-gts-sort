#! /usr/bin/perl -w

## Author: P. Sannes, IT-div
## Date: 2016-11-09
## This is a copy of merge_index.pl, with some special treatment for
## obstype drau

## Merge index file 1 with index file 2 into a new index file, where
## - all addresses (first field) in index file 2 have been increased
## with the size of index file 1, computed as the largest address in
## index file 1 pluss the length of the corresponding record
## - duplicates have been removed.

## This makes it possible to append bufr file 2 to bufr file 1 and use
## the merged index file as index file for the new bufr file.

## It is assumed that index file 1 and 2 are free of duplicates
## internally.

## Lines in index files are supposed to match:
## <address> <length> <updnum> <ncorr> <code>=<string> ...

## Two lines are considered duplicates if they have the same
## key = '<code>=<string> ...' part. The line kept will be the one with
## highest updnum or highest ncorr or biggest length (in this priority).
## If everything is equal, the line in index file 2 is kept (the line
## in index file 1 removed).

## Note: in a previous version $size1 was provided as argument to
## script, found by -s <bufr file 1> in calling script, but
## mysteriously this size sometimes proved to be wrong.

## Special for drau: when searching for duplicates, disregards what is
## found in BUOY?= part of key (equality for TIME and POS_H should be
## enough)

use strict;

my $Usage = "Usage: $0 <index file 1> <index file 2> <new index file>\n";

die $Usage unless $#ARGV == 2;

my ($index_file1, $index_file2, $new_index_file) = @ARGV;

my $size1 = get_size($index_file1);

my $index_reg_exp = qr/^ *(\d+) +(\d+) +(\d+) +(\d+) +(.*)$/;
my $key_reg_exp = qr/^(BUOY.?)=\d+ (.*)/;

open (IN1, '<', $index_file1) or die "Cannot open $index_file1: $!";
open (IN2, '<', $index_file2) or die "Cannot open $index_file2: $!";
open (OUT, '>', $new_index_file) or die "Cannot open $new_index_file: $!";

my @lines2 = <IN2>;

my $lines2_ref; # Hash to hold array index of the keys in index 2 file

my (@address2, @length2, @updnum2, @ncorr2, @key2, @buoy2, @rest_key2);
foreach my $i ( 0 .. $#lines2 ) {
  my ($address2, $length2, $updnum2, $ncorr2, $key2) =
    ( $lines2[$i] =~ $index_reg_exp );
  push @address2, $address2 + $size1;
  push @length2, $length2;
  push @updnum2, $updnum2;
  push @ncorr2, $ncorr2;
  push @key2, $key2;
  my ($buoy2, $rest_key2) = ($key2 =~ $key_reg_exp);
  push @buoy2, $buoy2;
  push @rest_key2, $rest_key2;
  if ( defined($lines2_ref->{$rest_key2}) ) {
    die "Duplicates exists for $index_file2 with key: $key2\nAborting $0\n";
  } else {
    $lines2_ref->{$rest_key2} = $i;
  }
}

# Step through index file 1 and check for duplicates by comparing with
# lines in index file 2, printing to new index file all records which
# should not be replaced by a record in index file 2 with the same key,
# and also removing duplicates in $lines2 hash.
while ( my $line1 = <IN1> ) {
  my ($address1, $length1, $updnum1, $ncorr1, $key1) =
    ( $line1 =~  $index_reg_exp );
  last if !$key1;
  my ($buoy1, $rest_key1) = ($key1 =~ $key_reg_exp);
  if ( defined($lines2_ref->{$rest_key1}) ) {
    # Duplicate. Decide which one to use:
    my $i = $lines2_ref->{$rest_key1};
    my $keep1;
    # For buoys updnum and ncorr are likely always 0, but doesn't hurt
    # to check for them. We disregard length of BUFR message, however
    if ( $updnum1 > $updnum2[$i] ) {
      $keep1 = 1;
    } elsif ( $updnum1 < $updnum2[$i] ) {
      $keep1 = 0;
    } elsif ( $ncorr1 > $ncorr2[$i] ) {
      $keep1 = 1;
    } elsif ( $ncorr1 < $ncorr2[$i] ) {
      $keep1 = 0;
    } elsif ( $buoy2[$i] eq 'BUOY7' ) {
      $keep1 = 0;
    } elsif ( $buoy1 eq 'BUOY7' ) {
      $keep1 = 1;
    } else {
      $keep1 = 0; # Use index file 2 if everything is equal
    }
    if ( $keep1 ) {
      delete $lines2_ref->{$rest_key1};
      print OUT $line1;
    }
  } else {
    print OUT $line1;
  }
}

# Then add the remaining records in index file 2 to new index file.
foreach my $i ( 0 .. $#lines2 ) {
  print OUT " $address2[$i] $length2[$i] $updnum2[$i] $ncorr2[$i] $key2[$i]\n"
    if defined($lines2_ref->{$rest_key2[$i]});
}


# Get size of bufr file 1 by adding the first two fields of the line in
# index file 1 which have the biggest first field
sub get_size {
    my $index_file = shift;

    open(IN, '<', $index_file1) or die "Cannot open $index_file1: $!";
    my $max_address = 0;
    my $len = 0;
    while (<IN>) {
	last if /^\s*$/;
        my ($address, $length) = (split ' ', $_)[0,1];
        if ( $address >= $max_address ) { # Use >= to get length of address=0 line
            $max_address = $address;
            $len = $length;
        }
    }
    close(IN) or die "Cannot close $index_file1: $!";

    return $max_address + $len;
}
