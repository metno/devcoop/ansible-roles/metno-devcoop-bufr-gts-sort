#! /usr/bin/perl -w

## Author: P. Sannes, IT-div
## Date: 2009-05-15

## Splits <index file> into several index files (in [workdir]),
## skipping lines containing '*' (indicating missing value for a key),
## and skipping lines with no TIME or position, and optionally
## skipping index lines referring to future or too far back in the
## past.

## Named for metar: meta_yyyymmddhhMM.idx with MM=00 or MM=30,
## containing all lines in <index file> matching TIME=yyyymmddHHMM +-
## 15 minutes, where the border cases HHMM = 1215 and 1245 are put
## into HHmm = 1230 and 1300, respectively.

## For other obstypes than metar: named as <obstype>_yyyymmddhh.idx,
## containing all lines in <index file> matching TIME=yyyymmddHH +- 30
## minutes, where the border case HHMM = 1230 is put into hh = 13.

## Note: the algorithm used does not guarantee that the original order
## in <index file> for records with same datetime is kept, although
## this actually seems to happen in practice (with some very few
## exceptions).

use Getopt::Long;
use POSIX qw(strftime);
use Time::Local qw(timegm timegm_nocheck);
use strict;

die usage() unless @ARGV;

my ($obstype, $index_file, $work_dir) = @ARGV;
$work_dir ||= '.';

# Parse command line options
my %option = ();
GetOptions(
           \%option,
           'skip_future_data',
           'list_future_data',
           'skip_too_old_data',
           'list_too_old_data',
           'logfile=s',
       )
    or die usage();

my $skip_future_data = $option{skip_future_data};
my $list_future_data = $option{list_future_data};
my $skip_too_old_data = $option{skip_too_old_data};
my $list_too_old_data = $option{list_too_old_data};
my $logfile = $option{logfile} || '';

# No time limit backwards for ocea
if ( $obstype eq 'ocea' ) {
    $skip_too_old_data = 0;
    $list_too_old_data = 0;
}

my $maxtime = 999912312359;
if ( $skip_future_data ) {
    # Calculate timestamp 1 hour into future
    my $seconds = 3600;
    $maxtime = strftime ("%Y%m%d%H%M", gmtime(time + $seconds));
}
my $mintime = 197012312359;
if ( $skip_too_old_data ) {
    # Calculate timestamp 3 days into past
    my $seconds = 3 * 24 * 3600;
    $mintime = strftime ("%Y%m%d%H%M", gmtime(time - $seconds));
}
my $nowtime = strftime ("%Y-%m-%d %H:%M:%S", gmtime(time));


# Where to direct output from (possible) check for future/too old data
my $LOG;
if ($logfile) {
    open($LOG, '>>', $logfile)
        or die "Cannot open $logfile for appending: $!";
} else {
    $LOG = *STDOUT;
}

open(IN, '<', $index_file) or die "Cannot open $index_file: $!";

my @lines = ( <IN> );
close IN;

# Remove lines with missing value for a key
@lines = grep {!/\*/} @lines;
# Remove lines with no value for TIME (these are probably NIL reports)
@lines = grep {/TIME=/} @lines;
# Remove lines with invalid datetimes
@lines = grep { valid_time($_) } @lines;
# Also remove lines with no position (in order not to cause problems
# for other systems)
@lines = grep {/POS_/} @lines;

# Sort on yyyymmddhh in TIME=yyyy... part of lines, with sort key
# added as second element in an anonymous array.
my @sorted_lines =
    sort { $a->[1] <=> $b->[1] }
    map { [$_, /TIME=(\d{12})/] } @lines;

# bufrupdate is not able to remove all duplicates for buoys, so
# complete the work here
if ( $obstype eq 'drau' ) {
    @sorted_lines = purge_drau(@sorted_lines);
}

# Then write sorted lines to correctly named index files
my $prev_time = 0;
my $OUT;
LOOP:
for ( 0 .. $#sorted_lines ) {
    my $timestamp = $sorted_lines[$_]->[1];

    # Index lines with TIME into future might need special treatment
    if ( $skip_future_data and $timestamp > $maxtime ) {
        if ( $list_future_data ) {
            print $LOG "[$nowtime $obstype Future data]  ", $sorted_lines[$_]->[0];
        }
        next LOOP if $skip_future_data;
    }

    # Index lines with TIME too long into past might need special treatment
    if ( $skip_too_old_data and $timestamp < $mintime ) {
        if ( $list_too_old_data ) {
            print $LOG "[$nowtime $obstype Too old data]  ", $sorted_lines[$_]->[0];
        }
        next LOOP if $skip_too_old_data;
    }

    my $new_time = ( $obstype eq 'meta' )
        ? round_time_for_metar($timestamp)
            : round_time($timestamp);
    if ( $new_time != $prev_time  && $new_time =~ /^\d{10}/) {
        $prev_time = $new_time;
        my $new_index_file = "$work_dir/$obstype" . '_' . $new_time . '.idx';
        open($OUT, '>>', $new_index_file) or
            die "Cannot open new index file $new_index_file: $!";
    }

    print $OUT $sorted_lines[$_]->[0] if $OUT;
}

###########################################################################

# Returns true (1) if TIME key is a valid datetime, else false (0)
sub valid_time {
    my $line = shift;
    my ($yyyymmddHHMM) = ($line =~ /TIME=(\d{12})/);
    return 0 unless $yyyymmddHHMM;
    my ($yyyy, $mm, $dd, $HH, $MM) = unpack "A4A2A2A2A2", $yyyymmddHHMM;
    eval { timegm(0, $MM, $HH, $dd, $mm-1, $yyyy) } || return 0;
    return 1;
}

# Returns argument rounded to nearest half hour, e.g. 200902282345 is
# rounded to 200903010000
sub round_time_for_metar {
    my $yyyymmddHHMM = shift;
    my ($yyyy, $mm, $dd, $HH, $MM) = unpack "A4A2A2A2A2", $yyyymmddHHMM;
    if ( $MM >= 45 ) {
        $MM = 00;
        # Add one hour
        my $time = timegm_nocheck(0, $MM, $HH, $dd, $mm - 1, $yyyy) + 3600;
        # Calculate new date and hour
        ($HH, $dd, $mm, $yyyy) = (gmtime($time))[2,3,4,5];
        $mm++;
        $yyyy += 1900;
    } elsif ( $MM >= 15 ) {
        $MM = 30;
    } else {
        $MM = 00;
    }
    return sprintf "%4d%02d%02d%02d%02d", $yyyy, $mm, $dd, $HH, $MM;
}

# Returns argument rounded to nearest hour, e.g. 200902282331 is
# rounded to 2009030100. No check that date is a valid date, so April
# 31th will be transformed as if it was May 1st, which actually is a
# nice way to correct date for those messages which have this month
# transition coding problem
sub round_time {
    my $yyyymmddHHMM = shift;
    my ($yyyy, $mm, $dd, $HH, $MM) = unpack "A4A2A2A2A2", $yyyymmddHHMM;
    if ( $MM >= 30 ) {
        # Add one hour, setting minute to 0
        my $time = timegm_nocheck(0, 0, $HH, $dd, $mm - 1, $yyyy) + 3600;
        # Calculate new date and hour
        ($HH, $dd, $mm, $yyyy) = (gmtime($time))[2,3,4,5];
        $mm++;
        $yyyy += 1900;
    }
    return sprintf "%4d%02d%02d%02d", $yyyy, $mm, $dd, $HH;
}

# Buoys may be identified in different ways
sub purge_drau {
    my @lines = @_;

    my $key_reg_exp = qr/^.*(BUOY.?)=\d+ (.*)/;
    my @indexes_of_kept_lines;
    my %seen = ();
    my $time = '';
    foreach my $i ( 0 .. $#lines ) {
	if ($lines[$i]->[1] ne $time) {
	    $time = $lines[$i]->[1];
	    push @indexes_of_kept_lines, sort map ($_->[0], values %seen);
	    %seen = ();
	}
	my $line = $lines[$i]->[0];
	my ($buoy, $key) = ($line =~ $key_reg_exp);
	# This shouldn't really happen
	next if !$key;
	# If duplicates: choose the already seen if BUOY7, else the last
	if ($seen{$key} && $seen{$key}->[1] eq 'BUOY7') {
	    next;
	} else {
	    $seen{$key} = [$i, $buoy];
	}
    }
    push @indexes_of_kept_lines, sort map ($_->[0], values %seen);

    return @lines[@indexes_of_kept_lines];
}

sub usage {
    print <<EOF;
Usage: $0 <obstype> <index file> [workdir]
        [--skip_future_data]
        [--list_future_data]
        [--skip_too_old_data]
        [--list_too_old_data]
        [--logfile <file>]
Works on BUFR index files created by bufrupdate.
Splits <index file> into several index files (in [workdir]).

Index lines with TIME more than 1 hour into future will be
skipped if --skip_future_data is set, and/or printed to STDOUT
if --list_future_data is set.
Index lines with TIME more than 3 days into past will be
skipped if --skip_too_old_data is set, and/or printed to STDOUT
if --list_too_old_data is set, except for ocea where these options
are ignored (no old data skipped or listed). If --logfile is set,
the printing to STDOUT mentioned above will instead go to <file>.
EOF
}
