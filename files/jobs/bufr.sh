#! /bin/bash

## Author: P. Sannes, IT-div
## Date: 2006-04-12
## 2020-02-09 modified for use at DoM Sri Lanka by Alexander Bürger

## PURPOSE:

## Exctracting bufr messages from msys9 into date specific files.

## USAGE:
## $0 <obstype> <ahl> <gtsfile> [(--compress | --use_subsets) [--sort]]

## DESCRIPTION:

## Extracts all messages with ahl = <ahl> from file <gtsfile> to
## $WORKDIR, into files <obstype>_<yyyymmddhh>.bufr according to
## datetime <yyyymmddhh> of the individual bufr messages, with
## corresponding index files <obstype>_<yyyymmddhh>.idx. Multisubset
## bufr messages are split into single subset messages, and multi
## observations in some cases split into single observations. The
## .bufr files in $WORKDIR are then appended to corresponding files in
## $DATADIR, with duplicates removed, and new index files created.

## Compressing of bufr files to $COMPRESSDIR is also included as an
## option, but there are difficulties here: $PURGEBUFR (or the
## software we use to read bufr files) has to be improved so as not
## to cause error messages by libbufr software if too many subsets
## are used.

## OUTPUT:

## $WORKDIR/<obstype>_<yyyymmddhh>.bufr
## $WORKDIR/<obstype>_<yyyymmddhh>.idx
## $DATADIR/<obstype>_<yyyymmddhh>.bufr
## $DATADIR/<obstype>_<yyyymmddhh>.idx
## $COMPRESSDIR/<obstype>_<yyyymmddhh>.bufr_comp

set -e  # Exit if a simple shell command fails

# see "man getopt" and /usr/share/doc/util-linux/examples/getopt-parse.bash
OPTIONS=$(getopt -o 't:a:f:w:csd:l:' --long 'type:,ahl:,file:,workdir:,compress,sort,datadir:,logdir:' -n "$0" -- "$@")
if [ $? -ne 0 ]; then
    echo 'Terminating...' >&2
    exit 1
fi
eval set -- "$OPTIONS"
unset OPTIONS

while true; do
    case "$1" in
        '-t'|'--type')    obstype="$2" ; shift 2 ; continue ;;
        '-a'|'--ahl')     ahl="$2"     ; shift 2 ; continue ;;
        '-f'|'--file')    gtsfile="$(realpath $2)" ; shift 2 ; continue ;;
        '-w'|'--workdir') WORKDIR="$2" ; shift 2 ; continue ;;
        '-d'|'--datadir') DATADIR="$2" ; shift 2 ; continue ;;
        '-l'|'--logdir')  LOGDIR="$2"  ; shift 2 ; continue ;;

        '-c'|'--compress') compress="--compress" ; shift ; continue ;;
        '-u'|'--use_subsets') compress="--use_subsets" ; shift ; continue ;;
        '-s'|'--sort') sort="yes" ; shift ; continue ;;

        '--') shift ; break ;;
        *) echo 'Internal error!' >&2 ; exit 1 ;;
        esac
done

if test "$#" != 0; then
    echo "Extra arguments found: $@"
    exit 1
fi

if test -z "$obstype" -o -z "$ahl" -o -z "$gtsfile" -o -z "$WORKDIR" -o -z "$DATADIR" -o -z "$LOGDIR"; then
    echo "Missing required argument(s)."
    exit 1
fi

case "$obstype" in
    amdar|drau|syno|temp|tide) ;;
    * )
	echo "Obstype '$obstype' is not supported. Aborting $0"
	exit 1
	;;
esac

# Files
SCRIPTS_DIR="$(dirname $0)"
SCRIPTS_DIR="$(realpath $SCRIPTS_DIR)"
CODES="$SCRIPTS_DIR/BF_codes"
DATA=bufr.dat
STORAGE=bufr.storage
INDEX=bufr.index

# Programs
BUFREXTRACT=bufrextract.pl
UPDBUFR=bufrupdate
PURGEBUFR=bufrpurge
SPLITINDEX="$SCRIPTS_DIR/splitindex.pl"
MERGE_INDEX="$SCRIPTS_DIR/merge_index.pl"
if [ $obstype = "drau" ]; then
    MERGE_INDEX="$SCRIPTS_DIR/merge_index_drau.pl"
fi
SORTBUFRTEMP="$SCRIPTS_DIR/sortbufrtemp.pl"

# Environment
if test -z "$BUFR_TABLES" ; then
    export BUFR_TABLES=/usr/local/lib/bufrtables/
fi
case "$BUFR_TABLES" in
    */) ;;
    *) BUFR_TABLES="$BUFR_TABLES/" ;;
esac

mkdir -p "$WORKDIR" "$DATADIR" "$LOGDIR"
if test -n "$compress" ; then
    mkdir -p "$COMPRESSDIR"
fi

# Clean up WORKDIR; cd to WORKDIR and stay there
rm -f $WORKDIR/*
cd $WORKDIR

# Get all new bufr messages into $DATA

# For BUFR bulletins it is better to use bufrextract.pl than ahlgrab,
# because then we will skip truncated BUFR messages (or more
# generally: BUFR messages with wrong length in section 0), which
# might cause problems in further processing
command="$BUFREXTRACT $gtsfile --ahl '$ahl' --outfile $DATA"
echo "Executing: $command"
# Adding 'eval' is necessary if $ahl contains spaces
eval $command || {
    echo "$command failed. Aborting $0"
    exit 1
}

if [ ! -s "$DATA" ]; then
    echo "No bufr messages extracted. Nothing more to do"
    exit 0
fi

# For TEMP BUFR we need to sort pressure levels
if [ $obstype = 'temp' ] ; then
    command="$SORTBUFRTEMP $DATA --outfile ${DATA}.tmp"
    echo "Executing: $command"
    $command || {
	echo "$command failed. Aborting $0"
	exit 1
    }
    mv ${DATA}.tmp $DATA
fi

touch $INDEX

# Process $DATA into a new bufr file $STORAGE where multi subset
# messages have been divided into single subset messages, with a
# corresponding index file $INDEX'_NEW' where duplicates have been
# removed from the index file (might still be present in $STORAGE)

command="$UPDBUFR $DATA $STORAGE $INDEX $CODES"
echo "Executing: $command"
$command || {
    echo "$command failed. Aborting $0"
    exit 1
}

mv $INDEX'_NEW' $INDEX

# Split the index file into separate index files for each datetime
# yyyymmddhh present in field 'TIME=...' in index file. The new index
# files are named <obstype>_<yyyymmddhh>.idx

options="--skip_future_data --list_future_data --logfile=$LOGDIR/future.log"
command="$SPLITINDEX $obstype $INDEX $WORKDIR $options"
if [ $obstype = 'syno' -o $obstype = 'drau' ] ; then
    command=$command" --skip_too_old_data --list_too_old_data"
fi
echo "Executing: $command"
$command || {
    echo "$command failed. Aborting $0"
    exit 1
}

# Process each index file created, one by one

for idx_file in *idx
  do
    # need this test because if no files match *idx,
    # $idx_file will be set to "*idx"
  if [ -f $idx_file ]
      then
      echo "Processing $idx_file"

# First: extract the Bufr messages from $STORAGE pointed to by
# this index file into a new bufr file, also updating index file
      bufr_file=`echo $idx_file | sed 's/\.idx$/.bufr/'`
      new_index_file=$idx_file'_NEW'
      option="--new_index $new_index_file"

      command="$PURGEBUFR $STORAGE $idx_file $bufr_file $option"
      echo "Executing: $command"
      $command || {
	  echo "$command failed. Aborting $0"
	  exit 1
      }
      mv $new_index_file $idx_file

# Then append this new bufr file to corresponding bufr file in $DATADIR.
      should_append='yes'
      if [ ! -f $DATADIR/$bufr_file ]
      then
	  # Double check, required since /opdata is NFS-mounted, so if
	  # file was created in a run on another machine, the file
	  # might not be discovered in this run unless doubly checked
	  if [ ! -f $DATADIR/$bufr_file ]
	  then
	      should_append='no'
	      echo "cp $bufr_file $idx_file $DATADIR"
	      cp $bufr_file $idx_file $DATADIR
	  else
	      echo "$(date +"%Y-%m-%d %H:%M:%S")"
	      echo "Now $DATADIR/$bufr_file is found (with -f)"
	      stat $DATADIR/$bufr_file
	      # Be sure idx file is also found
	      if [ ! -f $DATADIR/$idx_file ]
	      then
		  if [ ! -f $DATADIR/$idx_file ]
		  then
		      echo "However $DATADIR/$idx_file is not found!!"
		      echo "Aborting $0"
		      exit 1
		  fi
	      fi
	  fi
      fi

      if [ $should_append = "yes" ]
      then
	  # Append new bufr file to old
	  new_bufr_file=$bufr_file'_accum'
	  echo "cat $DATADIR/$bufr_file $bufr_file >> $new_bufr_file"
	  cat $DATADIR/$bufr_file $bufr_file >> $new_bufr_file
	  # Create the new index file
	  new_index_file=$idx_file'_accum'

	  command="$MERGE_INDEX $DATADIR/$idx_file $idx_file $new_index_file"
	  echo "Executing: $command"
	  $command || {
	      echo "$command failed. Aborting $0"
	      exit 1
	  }

# Purge bufr file by use of new index file
	  purged_bufr_file=$bufr_file'_purged'
	  purged_idx_file=$idx_file'_purged'
	  files="$new_bufr_file $new_index_file $purged_bufr_file"
	  option="--new_index $purged_idx_file"

	  command="$PURGEBUFR $files $option"
	  echo "Executing: $command"
	  $command || {
	      echo "$command failed. Aborting $0"
	      exit 1
	  }
	  rm $new_bufr_file $new_index_file
	  echo "mv $purged_bufr_file $DATADIR/$bufr_file"
	  mv $purged_bufr_file $DATADIR/$bufr_file
	  echo "mv $purged_idx_file $DATADIR/$idx_file"
	  mv $purged_idx_file $DATADIR/$idx_file
      fi

# Finally compress the bufr file, optionally first sorting index file
# for improved effect

      if [ -n "$compress" ] ; then
	  compressed_file=$bufr_file'_comp'
	  if [ -n "$sort" ] ; then
	      sorted_idx_file=$idx_file'_sorted'
	      sort -k2,2n -k1,1n $DATADIR/$idx_file > $sorted_idx_file
	      files="$DATADIR/$bufr_file $sorted_idx_file $compressed_file"
	  else
	      files="$DATADIR/$bufr_file $DATADIR/$idx_file $compressed_file"
	  fi
	  command="$PURGEBUFR $files $compress"
	  echo "Executing: $command"
	  $command || {
	      echo "$command failed. Aborting $0"
	      exit 1
	  }
	  mv $compressed_file $COMPRESSDIR
      fi
  fi
done
